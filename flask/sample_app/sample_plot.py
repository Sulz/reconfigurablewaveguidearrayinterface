import cStringIO
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg


class Test():
    def __init__(self):
        pass

    def make_plot(self):
        fig = Figure(figsize=[4,4])
        ax = fig.add_axes([.1,.1,.8,.8])
        ax.scatter([1,2], [3,4])
        canvas = FigureCanvasAgg(fig)

        # write image data to a string buffer and get the PNG image bytes
        buf = cStringIO.StringIO()
        canvas.print_png(buf)

        return buf.getvalue()
