import numpy as np
import scipy.linalg as scipy
import itertools as it
import matplotlib.pyplot as plt
import math

import matplotlib.pylab as py_plt
from matplotlib import gridspec
from matplotlib.pylab import cm, mpl
from mpl_toolkits.mplot3d import Axes3D


class Simulator():
    def __init__(self, propagation_const, coupling_coeff, input_photons, structure_length):
        assert len(propagation_const) == len(input_photons), \
            "\nEnsure a photon exists, or not, for each waveguide.\n"

        assert len(coupling_coeff) == len(propagation_const) - 1, \
            "\nToo many coupling coefficiants. \n" + \
            "Ensure one less than number of waveguides.\n"

        self.propagation_consts = propagation_const
        self.sliced_structure_length = 0

        # TODO
            # Clean up the coupling coefficient definition method.

        self.coupling_coeffs = []
        for each in coupling_coeff:
            self.coupling_coeffs.append(each*1E6)

        # self.coupling_coeffs = coupling_coeff
        # print self.coupling_coeffs
        # done = 0
        # while not done:
        #     pass

        self.source_photons = input_photons
        self.structure_length = structure_length

        self.num_photons = 0
        for _ in input_photons:
            if _:
                self.num_photons += 1

        print "\nSimulating structure with following features"
        print " Structure length:", structure_length, "meters"
        print " Number of photons:", self.num_photons
        print " Number of waveguides:", len(self.propagation_consts)
        print " Coupling: ", coupling_coeff[0]

    # Can either do each step from external, or simply pass simulator the
        # parameters once, and return the plot.

        # hamiltonian = self.generate_hamiltonian(propagation_const, coupling_coeff)

        # unitary = self.make_unitary(hamiltonian, input_photons)

        # prob_amps = self.simulate(unitary, input_photons)

        # probabilities = self.get_probabilities(prob_amps)

        # self.plot_results(probabilities)

    def generate_hamiltonian(self, prop_const, coupling_coeff):
        hamiltonian = np.zeros((len(prop_const), len(prop_const)))

        for i in range(0, len(prop_const)):
            hamiltonian[i][i] = prop_const[i]
            if i == 0:
                hamiltonian[i][1] = self.coupling_coeffs[i]

            elif i == len(prop_const) - 1:
                hamiltonian[i][i - 1] = self.coupling_coeffs[i - 1]

            else:
                hamiltonian[i][i - 1] = self.coupling_coeffs[i - 1]
                hamiltonian[i][i + 1] = self.coupling_coeffs[i]

        # print '\nHamiltonian:\n', hamiltonian
        return hamiltonian


    def time_evolve(self, hamiltonian, source, structure_length):

        print "\n\ntime evolving with structure length:", structure_length

        unitary = scipy.expm(-1j * hamiltonian * structure_length)

        U = [1]
        for i in range(self.num_photons):
            U = np.kron(U, unitary)
            # print U.shape

        unitary = U

        if self.num_photons > 1:
            source = self._seperate_sources(source)
            print "Shape of photon source state:", source.shape
            print "\n\n"


        if self.num_photons == 1 and self.sliced_structure_length == 0:
            self.get_propagation_data(U, hamiltonian, source)

        # print "\nShape of structure unitary:", unitary.shape
        return unitary, source

    def _seperate_sources(self, source):
        print "\nInitial source definition:\n", source
        src = np.zeros((self.num_photons, len(self.propagation_consts)))
        index = 0
        for i in range(len(source)):
            if source[i] == 1:
                src[index][i] = 1
                j = i + 1
                while j < len(self.propagation_consts):
                    src[index][j] = 0
                    j += 1
                index += 1

            if index < self.num_photons:
                src[index][i] = 0

        final_src = []
        for i in range(self.num_photons):
            elements = []
            for j in range(len(self.propagation_consts)):
                elements.append([src[i][j]])
                # print "elements:\n", elements
            final_src.append(np.matrix(elements))

        return self._generate_source_state(final_src)
        # return self._source_tensor_sum(src)

    def _generate_source_state(self, photons):

        input_photon_combinations = list(it.permutations(photons))
        psi_in_combinations = []

        for comb in input_photon_combinations:
            psi_in = [1]

            for p in comb:
                psi_in = np.kron(psi_in, p)

            psi_in_combinations.append(psi_in)

        psi_in = np.matrix(np.sum(psi_in_combinations, 0) / np.sqrt(np.sum(psi_in_combinations)))
        psi_in = psi_in.transpose()
        return psi_in

    def simulate(self, unitary, photons):
        probability_amplitudes = np.dot(photons, unitary)
        return probability_amplitudes

    def get_probabilities(self, prob_amps):
        probabilities = []

        if self.num_photons == 1:
            for each in prob_amps:
                val = np.absolute(each)**2
                probabilities.append(val)

            # print 'prob_amps', prob_amps
            # print 'len(probabilities):', len(probabilities)
            # print 'probabilities\n', probabilities
        else:
            probabilities = np.absolute(prob_amps)
            probabilities = np.square(probabilities)
            probabilities = probabilities.tolist()[0]

        return probabilities

    def plot_results(self, probabilities):
        if self.num_photons == 2:
            correlation_matrix = self._calculate_correlation_matrix(probabilities, self.num_photons, len(self.propagation_consts))
            self._plot_correlation_matrix(correlation_matrix)

        # self.plot_3(probabilities)
        self.plot_2(probabilities)

    def get_propagation_data(self, og_unitary, hamiltonian, source):
        slices = 10
        self.sliced_structure_length = self.structure_length / slices

        # prop_source = np.ndarray((slices), int)
        photon_propagations = []
        photon_propagations.append(source)

        prop_unitary = [[] for i in range(slices + 1)]
        # prop_unitary[0] = source

        # n simulations where the structure_length = (i * structure_length)/ n
        for i in range(1, slices + 1):
            # print '\n\nself.structure_length/(i * slices)', float(i * self.structure_length / slices)
            prop_unitary[i], temp = self.time_evolve(hamiltonian, source, float(i * self.structure_length / slices))

            results = self.simulate(prop_unitary[i], source)
            results = self.get_probabilities(results)

            # Hold results of simulation i, ie result[1] holds probabilities from original source and (1 * structure_length)/10
            photon_propagations.append(results)

        # print '\n\nlen(photon_propagations)', len(photon_propagations)
        # print "photon_propagations\n", photon_propagations
        # for each in photon_propagations:
        #     print '\nlen(each):', len(each)
        #     print 'sum(each)', sum(each)
            # print "each:\n", each

        # print "\n\n"

        self.plot_single_photon_propagation(photon_propagations)


    # __author__ = 'Robert Chapman - QPL, RMIT University'
    def plot_single_photon_propagation(self, array_class, title=''):
        return

        plt.close("all")  # Close a figure window

        array_class = np.array(array_class)
        # print '\n\ntype(array_class)', type(array_class)
        # print 'array_class', array_class
        # print "\n\n"

        distribution_steps = array_class.T

        x = np.linspace(0, array_class.length, len(distribution_steps[0]) + 1)

        fig = plt.figure(figsize=(15, 6))
        plt.suptitle(title, fontsize=16)
        gs = gridspec.GridSpec(1, 2, width_ratios=(3, 1))
        ax = plt.subplot(gs[0])
        ax2 = plt.subplot(gs[1])

        x = np.arange(distribution_steps.shape[1])
        y = np.arange(1 / 2 - distribution_steps.shape[0], 1 + distribution_steps.shape[0])

        length = len(distribution_steps)

        data = []
        data.append(np.zeros(len(distribution_steps[0])))
        for i in range(length):
            data.append(distribution_steps[i])
            data.append(np.zeros(len(distribution_steps[0])))

        ax.contourf(x, y, data, 200, cmap=plt.cm.jet, extends='max')

        x_text = np.linspace(0, array_class.length, 5)
        x_text = [str(a) for a in x_text]

        y_ticks = np.linspace(-len(self.propagation_const) + 1, len(self.propagation_const) - 1, 2 * len(self.propagation_const) - 1)
        ax.set_yticks(y_ticks)
        y_text = []
        y_labels = np.linspace(0, len(self.propagation_const) - 1, len(self.propagation_const))

        for i in range(len(self.propagation_const) - 1):
            y_text.append(int(y_labels[i]))
            y_text.append('')
        y_text.append(int(y_labels[-1]))
        ax.set_yticklabels(y_text)
        ax.set_xticks(np.linspace(0, x[-1], 5))
        ax.set_xticklabels(x_text)
        ax.set_xlabel('Propagation distance')
        ax.set_ylabel('Waveguide')
        ax.set_title('Waveguide Array Propagation')
        for t in ax.xaxis.get_ticklines(): t.set_visible(False)
        for t in ax.yaxis.get_ticklines(): t.set_visible(False)

        width = 0.8

        if len(self.propagation_const) % 2 != 0:

            ax2.barh(np.linspace(-int((len(self.propagation_const)) / 2.), int((len(self.propagation_const)) / 2.), len(self.propagation_const)) - width / 2.,
                     np.array(array_class.output_distribution), width)

            ax2.set_yticks(range(-int(1 + (len(self.propagation_const) - 1) / 2.), int(len(self.propagation_const) - 1 / 2.)))

            ax2.set_yticklabels(range(-1, len(self.propagation_const)))

            ax2.set_ylim(0.5 - int(1 + (len(self.propagation_const) - 1) / 2.), 0.5 + int((len(self.propagation_const) - 1) / 2.))

            ax2.set_xlabel('Output Intensity')
            ax2.set_ylabel('Waveguide')
            ax2.set_title('Waveguide Array Output')

            for t in ax2.xaxis.get_ticklines(): t.set_visible(False)
            for t in ax2.yaxis.get_ticklines(): t.set_visible(False)

        else:

            ax2.barh(np.linspace(-int((len(self.propagation_const)) / 2.) + 1, int((len(self.propagation_const)) / 2.), len(self.propagation_const)) - width / 2,
                     np.array(array_class.output_distribution), width)
            ax2.set_yticks(np.linspace(-int((len(self.propagation_const)) / 2.), int((len(self.propagation_const)) / 2.), len(self.propagation_const) + 1))
            ax2.set_yticks(np.linspace(0, len(self.propagation_const), len(self.propagation_const) + 1))
            ax2.set_ylim(0.5 - int(1 + (len(self.propagation_const) - 1) / 2.), 0.5 + int((len(self.propagation_const)) / 2.))

            ax2.set_xlabel('Output Intensity')
            ax2.set_ylabel('Waveguide')
            ax2.set_title('Waveguide Array Output')

            for t in ax2.xaxis.get_ticklines(): t.set_visible(False)
            for t in ax2.yaxis.get_ticklines(): t.set_visible(False)

        # ax2.set_xlim(0, 1)

        return plt



    def plot_3(self, probabilities):
        """coordinate for every probability"""
        # plt.hist(probabilities)
        # plt.hexbin(x, probabilities)

        x = [i for i in range(0, len(probabilities))]
        plt.bar(x, probabilities)

        if self.num_photons > 1:
            new_x = []
            for i in range(len(x)):
                for j in range((len(x)/len(self.propagation_consts))):
                        new_x.append((x[i], x[j]))
            for i in range(len(x)):
                x[i] = x[i] + 0.4

            plt.xticks(x, new_x)

        else:
            new_x = x
            plt.xticks(x, new_x)

        axes = plt.gca()
        axes.set_xlim([0, len(x)])
        axes.set_ylim(0, max(probabilities) + 0.1 * max(probabilities))

        plt.show()

    def plot_2(self, probabilities):
        """Combined coordinates like (0, 1) and (1, 0) probabilities"""
        x_axis = []
        g = it.product(range(len(self.propagation_consts)), repeat=self.num_photons)
        for each in g:
            if len(each) == 1:
                x_axis.append(each[0])
            else:
                x_axis.append(each)

        if self.num_photons == 1:
            final_x = x_axis
            plt.bar(range(len(final_x)), probabilities, align='center')
            plt.xticks(range(len(final_x)), final_x, size='small')

            axes = plt.gca()
            axes.set_xlim([0, len(final_x)])
            axes.set_ylim(0, max(probabilities) + 0.1 * max(probabilities))

            plt.show()

        else:
            vals = {}
            for i in range(len(probabilities)):
                vals[str(x_axis[i])] = probabilities[i]

            another_x = []
            combine = []
            for each in x_axis:
                novel = 1
                for i in range(len(x_axis[0]) - 1):
                    if each[i] > each[(i + 1)]:
                        novel = 0
                if novel:
                    another_x.append(each)
                else:
                    combine.append(each)

            for each in combine:
                for _ in another_x:
                    if sorted(each) == sorted(_):
                        vals[str(_)] += vals[str(each)]

            coord_vals = []
            for each in another_x:
                coord_vals.append(vals[str(each)])

            final_x = []
            for each in another_x:
                final_x.append(str(each))

            plt.bar(range(len(final_x)), coord_vals, align='center')
            plt.xticks(range(len(final_x)), final_x, size='small')

            axes = plt.gca()
            axes.set_xlim([0, len(final_x)])
            axes.set_ylim(0, max(coord_vals) + 0.1 * max(coord_vals))

            plt.show()

    """For two photons"""
    def _calculate_correlation_matrix(self, output_state, no_photons, no_waveguides):

        # elements = [[] for i in range(len(output_state))]
        elements = []
        # print 'elements', elements
        another_out = []
        for i in range(len(output_state)):
            elements.append([output_state[i]])

        another_out.append(np.matrix(elements))
        # print '\nanother_out\n', another_out[0]
        output_state = another_out[0]

        if no_photons == 1:
            print('No correlation matrix with 1 photon')
            return 0

        elif no_photons == 2:

            # Calculates the correlation matrix for two photon output.
            # N-photon output requires an N dimensional correlation matrix to plot

            rho = output_state * output_state.H

            correlation_matrix = np.reshape([float(rho[i, i]) for i in range(len(rho))],
                                            (no_waveguides, no_waveguides))

            return correlation_matrix

    def _plot_correlation_matrix(self, corr, title='', axis_start=0):

        # print 'corr\n', corr
        # print type(corr)

        corr_real_array = np.array(corr)

        dim = len(corr)

        labels = []
        for i in range(dim - 1):
            labels.append(axis_start)

            axis_start += 1
        labels.append(axis_start)

        fig = py_plt.figure(figsize=(20, 10))

        py_plt.suptitle(title, fontsize=16)
        gs = gridspec.GridSpec(5, 13)
        ax = py_plt.subplot(gs[0:5, 0:13], projection='3d')

        # print '\n\ncorr_real_array\n', corr_real_array

        lx = len(corr_real_array)  # Work out matrix dimensions
        ly = len(corr_real_array[0])
        # xpos = np.arange(0,lx*0.7,0.7) # Set up a mesh of positions
        # ypos = np.arange(0,ly*0.7,0.7)
        xpos = np.linspace(0, lx * 0.7, lx)
        ypos = np.linspace(0, ly * 0.7, ly)
        xpos, ypos = np.meshgrid(xpos + 0.25, ypos + 0.25)

        xpos = xpos.flatten()  # Convert positions to 1D array
        ypos = ypos.flatten()
        zpos = np.zeros(lx * ly)
        dx = 0.5 * np.ones_like(zpos)
        dy = dx.copy()
        dz = corr_real_array.flatten()
        nrm = mpl.colors.Normalize(-np.max(corr_real_array), np.max(corr_real_array))
        colors = cm.RdBu(nrm(-dz))
        real_dz_max = max(dz)
        for i in range(len(xpos)):
            ax.bar3d(xpos[i], ypos[i], zpos[i] + 0.1, dx[i], dy[i], dz[i], color=colors[i], linewidth=0.0)

        ax.w_xaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
        ax.w_yaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
        ax.w_zaxis.set_pane_color((1.0, 1.0, 1.0, 1.0))
        dim = len(corr[0]) * 0.7
        ax.view_init(elev=20, azim=-55)
        ax.grid(False)
        ax.set_xticks(np.linspace(0.5, 0.7 * len(corr) + 0.5, len(corr)))
        ax.set_xticklabels(labels, ha='center', size=15, verticalalignment='bottom')
        ax.set_yticks(np.linspace(0.5, 0.7 * len(corr) + 0.5, len(corr)))
        ax.set_yticklabels(labels, ha='center', size=15, verticalalignment='bottom')
        for t in ax.xaxis.get_ticklines(): t.set_visible(False)
        for t in ax.yaxis.get_ticklines(): t.set_visible(False)

        py_plt.show()

    def _check_unitary(self, unitary):
        transposed_unitary = (unitary.transpose()).conjugate()
        identity = np.dot(unitary, transposed_unitary)

        identity = identity.round(10)

        print 'identity:' , identity
