from flask import Flask
from flask import json
from flask import jsonify
from flask import render_template
from flask import request
from flask_bootstrap import Bootstrap
app = Flask(__name__)


@app.route("/")
def index():
    return render_template('qWalker.html')

if __name__ == "__main__":
    app.run()


@app.route("/documentation")
def show_docs():
    return render_template('documentation.html')

@app.route("/_simulate")
def sim():
    print "\n\n\n\nBack at the server\n\n\n\n"
    print 'request.args', request.args


    # return jsonify(result = 0)

    found = 0
    for each in request.args:
        if each == 'beta[]':
            found = 1
            break

    if not found:
        print "\n\n\nNo data!!\n\n\n"
        return jsonify(result = 0)

    else:
        from flask import Flask, make_response
        from Simulator_Class import Simulator
        from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
        from matplotlib.figure import Figure
        beta = []
        for each in request.args.getlist('beta[]'):
            beta.append(int(each) * 1e3)

        num_wg = len(beta)

        coupling = []
        for each in request.args.getlist('coupling[]'):
            coupling.append(float(each))

        source = []
        for each in request.args.getlist('photons[]'):
            source.append(int(each))

        struct_length = [each for each in request.args.getlist('length')][0]
        # print '\n\ntype(struct_length)', type(struct_length)

        # print '\n\nstruct_length', struct_length

        struct_length = float(struct_length) * 1e-3
        # print '\n\nstruct_length', struct_length, "\n\n"

        if not struct_length:
            # print "\n\nhere\n\n"
            struct_length = 30e-3

        # print '\n\nstruct_length', struct_length

        num_photons = 0
        real_src = []
        for i in range(num_wg):
            real_src.append(source[i])
            if source[i]:
                num_photons += 1

        simulation = Simulator(beta, coupling, real_src, struct_length)
        hamiltonian = simulation.generate_hamiltonian(beta, coupling)

        # Could let user define a hamiltonian, and use here.
        unitary, input_photons = simulation.time_evolve(hamiltonian, real_src, struct_length)
        probability_amps = simulation.simulate(unitary, input_photons)
        probabilities = simulation.get_probabilities(probability_amps)


        prob_sum = 0
        for each in probabilities:
            prob_sum += each
        print "Sum of probs:", prob_sum, "\n"

        if num_photons > 2:
            axis, probabilities = simulation.plot_results(probabilities)
            # print 'axis', axis
            response = str(probabilities) + "__axis__" + str(axis)
            return response
            return jsonify(probabilities)

        if num_photons == 1:
            prop_plot, histogram = simulation.plot_results(probabilities)

            print 'probabilities', probabilities

            response = str(probabilities) + "_unique_breaker_" + str(prop_plot)

            return response

        elif num_photons == 2:
            corr_matrix, axis = simulation.plot_results(probabilities)
            # print 'probabilities:\n', probabilities
            response = str(probabilities) + "_unique_breaker_" + str(corr_matrix) + "__axis__" + str(axis)
            return response
