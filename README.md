# Quantum Random Walk Simulator
Quantum Walk simulator developed for my honors project.
Won third prize at the EnGenius trade fair 2017.

The Quantum Random Walk is the quantum equivalent to the classical random walk.
Classical Random Walks result with a Gaussian distribution pattern, whereas Quantum Random Walks result with a ballistic distribution.

The Quantum Random Walk can be used for improved random number generation, is used in some quantum algorithms, models energy transfer in photosynthesis, and is even a even model for universal quantum computation.

This project is based on a reconfigurable waveguide array designed and fabricated by QPLab (QPLab.org).

## Whats in the box
Visiting the interface webpage will allow you to define your own variable waveguide array, and simulate varying photon configurations.
The simulator allows you to set any parameters you want across a variable number of waveguides and see what happens when you inject photons into such a structure.

##### Results
The results are presented differently depending on the type of simulation.
Single photon simulations provide a propagation plot, visualizting the transfer of energy throughout the structure. 
An interactive horizontal bar chart will also be provided, indicating the probabilities of finding the single photon at any given output.

Two photon simulations will yield a interactive bar chart indicating the chance of finding the two photons in any combination of outputs.
A static histogram will also be presented providing a different visualization of the same percentages as the plot mentioned above.

More than two photon simulations yield just the interactive bar chart showing the chances of finding the photons at any arrangement of outcomes.

### Structure.
The project is comprised of two main parts, the frontend webpage which interfaces to the backend simulator.
The project is written in python where possible for the convenience of the group that fabricated the waveguide array.

The frontend is constructed using the Flask framework, and the backend is just python code.

The repo currently contains all code in the development of this project (including loooong obsolete code!).


## How to run
Before running the commands below, be sure to install the dependencies!
This is required as the project is not published, and thus your machine will act as the server.

First navigate to the flask_js_plots branch. There you will find the flask/ directory.

Move into qWalker/ and run using the following commands:

### Starting the server and running the simulations!
Navigating to the flask_js_plots branch, you will find the flask/ directory.
Move into qWalker/ and run using the following commands:

```
export FLASK_APP=interface.py
export DEBUG=1
flask run
```

The console will prompt you to visit your localhost via port 5000.
Open a browser and go to 127.0.0.1:5000

Enjoy!

### Dependencies
The following table contains all of the dependencies, and how to install them.

| Module | Command to install | Brief description |
| ------ | ------------------ | ----------------- |
| Flask | pip install flask | A python framework. Used for the webpage frontend. |
| flask_bootstrap | pip install flask_bootstrap | Bootstrap styling for the frontend interface. |
| Numpy | pip install numpy | A math library for the simulation backend. |
| Scipy | pip install scipy | The backend only uses the kroneker function from the linalg library of scipy. |
| Matplotlib | pip install matplotlib | Creates both the single photon propagation plot and histogram, which is then sen to frontend. |
