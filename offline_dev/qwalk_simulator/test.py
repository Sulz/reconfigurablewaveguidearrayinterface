import numpy as np
import scipy.linalg as scipy

import matplotlib.pyplot as plt

M = [
     [0., 5000.]
     [5000., 0.]
    ]

U = scipy.expm(-1j * M * 780E-6)

print U
